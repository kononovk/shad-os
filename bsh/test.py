#!/usr/bin/env python3

import os
import subprocess

SHELL_PATH = "./shell"

class Shell:
    def __init__(self, path):
        self._path = path

    def feed(self, string):
        shell = subprocess.Popen(self._path, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, encoding="utf8")
        stdout, stderr = shell.communicate(string + "\n", timeout=3)
        if "Sanitizer" in stderr:
            raise AssertionError("Sanitizer error:\n{}".format(stderr))
        assert shell.returncode == 0
        return stdout.replace("$ ", "").strip()

def test_shell_basics():
    shell = Shell(SHELL_PATH)

    assert not shell.feed("")
    assert shell.feed("echo hello") == "hello"
    assert shell.feed(" echo    hello  world") == "hello world"
    assert shell.feed("echo hello\necho world") == "hello\nworld"

    assert not shell.feed("touch ./foo")
    assert os.path.exists("./foo")
    assert not shell.feed("rm ./foo")
    assert not os.path.exists("./foo")

    assert shell.feed("touch ./bar\ncat ./bar\nwc -c bar\nrm ./bar") == "0 bar"
    assert not os.path.exists("./bar")

    assert shell.feed("cat\nhello\nworld") == "hello\nworld"
    assert shell.feed("./shell\necho hi\n./shell\necho hello") == "hi\nhello"

    assert shell.feed("cat /sys/proc/foo/bar") == ""

    assert shell.feed("foobar") == "Command not found"

def test_shell_redirection():
    shell = Shell(SHELL_PATH)

    assert not shell.feed("echo shad rocks > aaa")
    assert shell.feed("cat <aaa") == "shad rocks"
    assert not shell.feed("cat >/dev/null < aaa")

    assert not shell.feed("echo c forever >aaa")
    assert shell.feed("cat < aaa") == "c forever"

    assert not shell.feed("echo >aaa andrew tanenbaum")
    assert shell.feed("<aaa cat") == "andrew tanenbaum"

    assert not shell.feed("> aaa cat\nhello\nworld")
    assert shell.feed("< aaa cat") == "hello\nworld"

    assert not shell.feed(">aaa head -c 250 < /dev/zero")
    assert not shell.feed("wc > bbb -c aaa")
    assert shell.feed("< bbb cat") == "250 aaa"

    assert not shell.feed("rm aaa")
    assert not shell.feed("rm bbb")

    assert shell.feed("echo test foo bar>bbb") == "test foo bar>bbb"
    assert shell.feed("echo test<aaa>bbb") == "test<aaa>bbb"

    assert not shell.feed("echo >lol<wut alpha")
    assert shell.feed("cat lol<wut") == "alpha"
    assert not shell.feed("rm lol<wut")

    assert shell.feed("echo >foo > bar") == "Syntax error"
    assert shell.feed("cat <one > two <bar") == "Syntax error"
    assert shell.feed("wc -l <") == "Syntax error"
    assert shell.feed("cat < >hello") == "Syntax error"
    assert shell.feed("cat >>hello") == "Syntax error"

    assert shell.feed("</dev/zero cat > /sys/proc/foo/bar") == "I/O error"
    assert shell.feed("cat < /foo/bar/baz") == "I/O error"
    assert shell.feed("cat < baz > foo") == "I/O error"

if __name__ == "__main__":
    test_shell_basics()
    test_shell_redirection()
