#include "plugin.h"
#include <stdlib.h>

struct chunk {
    char data[1024];
    size_t len;
    struct chunk* prev;
};

void reverse(struct stream *stream)
{
    struct chunk *tail = malloc(sizeof(struct chunk));
    tail->prev = NULL;

    while ((tail->len = stream_read(stream, tail->data, sizeof(tail->data))) == sizeof(tail->data)) {
        struct chunk *prev = tail;
        tail = malloc(sizeof(struct chunk));
        tail->prev = prev;
    }

    while (tail) {
        char rev[sizeof(tail->data)];
        for (int i = 0; i < tail->len; ++i) {
            rev[i] = tail->data[tail->len - i - 1];
        }
        stream_write(stream, rev, tail->len);
        struct chunk *prev = tail->prev;
        free(tail);
        tail = prev;
    }
}

void uefa_plugin_encode(struct stream *stream)
{
    reverse(stream);
}

int uefa_plugin_decode(struct stream *stream)
{
    reverse(stream);
    return 0;
}
